package src;

import java.io.FileInputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        StringBuilder st = new StringBuilder();
        try(FileInputStream inputStream = new FileInputStream("/home/sharipov/IdeaProjects/new-git/file/text.text");) {
            int c;
            while ((c = inputStream.read())!=-1) {
                st.append((char)c);
            }

            int sum=0;
            String[] str = st.toString().split(" ");
            for (String s : str) {

                if(s.contains("you")) {
                    System.out.println(s);
                    sum++;
                }
            }

            System.out.println(sum);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}